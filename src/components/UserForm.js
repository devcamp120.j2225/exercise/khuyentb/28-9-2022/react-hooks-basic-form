import { useEffect, useState } from "react";

function UserForm () {
    const [firstName, setFirstName] = useState(localStorage.getItem("firstName"));
    const [lastName, setLastName] = useState(localStorage.getItem("lastName"));

    const handleFirstNameChange = (event) => {
        console.log(event.target.value);
        setFirstName(event.target.value)
    }

    const handleLastNameChange = (event) => {
        console.log(event.target.value);
        setLastName(event.target.value)
    }

    useEffect (() => {

        localStorage.setItem("firstName", firstName);
        localStorage.setItem("lastName", lastName);
    })
    return (
        <>
        <div><input placeholder="Devcamp" onChange={handleFirstNameChange} value={firstName}></input></div>
        <div><input placeholder="user" onChange={handleLastNameChange} value={lastName}></input></div>
        <p>{firstName} {lastName}</p>
        </>
    )
}

export default UserForm;